import { Component, OnInit } from '@angular/core';
import { HeroService } from 'src/app/services/hero.service';
import { Hero } from '../hero';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-one-hero-detail',
  templateUrl: './one-hero-detail.component.html',
  styleUrls: ['./one-hero-detail.component.scss']
})
export class OneHeroDetailComponent implements OnInit {

  hero: Hero = {name: '', id: 0};

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private location: Location
    ) { }

  ngOnInit() {
    this.getHero();
  }

  getHero(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
      .subscribe(heroes => this.hero = heroes);
  }

  goBack(): void {
    this.location.back();
  }

  saveName(): void {
    this.heroService.updateHero(this.hero)
      .subscribe(() => this.goBack());
  }

  deleteHero(hero: Hero): void {
    this.heroService.deleteHero(hero).subscribe();
    this.goBack();
  }
}
