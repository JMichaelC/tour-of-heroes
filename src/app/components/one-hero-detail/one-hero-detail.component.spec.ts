import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OneHeroDetailComponent } from './one-hero-detail.component';

describe('OneHeroDetailComponent', () => {
  let component: OneHeroDetailComponent;
  let fixture: ComponentFixture<OneHeroDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OneHeroDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OneHeroDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
