import { Hero } from './../hero';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.scss']
})
export class HeroDetailComponent implements OnInit {

  @Input()
  hero: Hero;

  constructor(private router: Router) { }

  onClickHide() {
    this.hero = undefined;
  }

  goToHero(id: number) {
    this.router.navigate([`detail/${id}`]);
  }

  ngOnInit() {
  }

}
