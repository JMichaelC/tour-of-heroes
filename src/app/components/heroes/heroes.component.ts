import { Hero } from '../hero';
import { Component, OnInit } from '@angular/core';
import '../hero-detail/hero-detail.component';
import { HeroService } from 'src/app/services/hero.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  // hero: Hero = {
  //   name: 'Superman',
  //   id: 12341234
  // };
  selectedHero: Hero;
  heroes: Hero[];

  constructor(private heroService: HeroService) { }

  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe((heroesFromService) => this.heroes = heroesFromService);
  }

  onClickHero(clickedHero: Hero) {
    this.selectedHero = clickedHero;
  }

  ngOnInit() {
    this.getHeroes();
  }

  add(name: string): void{
    name = name.trim();
    if (!name) {
      return;
    }
    this.heroService.addHero({name} as Hero)
      .subscribe(hero => this.heroes.push(hero));
  }

}
